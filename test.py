import unittest
import logging
import coloredlogs
import sys

from pots_user.tests.user_tests import UserTests
from pots_home.tests.create_tests import CreateTests

coloredlogs.install()


logging.basicConfig(stream=sys.stdout, level=logging.DEBUG,
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')

if __name__ == "__main__":
    unittest.main()
