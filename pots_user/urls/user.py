from django.urls import path
from pots_user.views.user import UserAPIView

urlpatterns = [
    path('profil/', UserAPIView.as_view(), name='update-profile')
]