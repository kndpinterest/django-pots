from django.apps import AppConfig


class PotsUserConfig(AppConfig):
    name = 'pots_user'
