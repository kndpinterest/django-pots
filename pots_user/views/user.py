from rest_framework import status, parsers, permissions
from rest_framework.views import APIView
from rest_framework.response import Response
from pots_user.serializer.user_serializer import UserSerializer
from django.contrib.auth.models import User
from django.utils.translation import gettext as _


class UserAPIView(APIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    parser_classes = [parsers.JSONParser, parsers.MultiPartParser]

    def post(self, request):
        user = request.user
        serializer = self.serializer_class(data=request.data)
        serializer.context['user'] = user
        if serializer.is_valid():
            serializer.save()
            message = serializer.context.get('message')
            return Response({'message': message}, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
