import logging
import unittest
from django.contrib.auth.models import User
from rest_framework.test import APIClient
from rest_framework import status
import dotenv
import os
from django.urls import reverse
from rest_framework_jwt.settings import api_settings
from django.core.files import File

jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER

dotenv.load_dotenv()


class UserTests(unittest.TestCase):
    def setUp(self):
        self.e = APIClient()
        self.payload = jwt_payload_handler(User.objects.first())
        self.encode = jwt_encode_handler(self.payload)

    @unittest.skipIf(User.objects.count() == 0, "User not have data")
    def test_login(self):
        urls = reverse('authtoken')
        data = {
            'username': os.environ.get('username'),
            'password': os.environ.get('password')
        }
        response = self.e.post(urls, data, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertNotEqual(response.data, None)
        log = logging.getLogger(__name__)
        log.info(response.data)

    @unittest.skipIf(User.objects.first().authenticate_set.first() == None, "User Authenticate Not have data")
    def test_update_profil(self):
        urls = reverse('update-profile')
        self.e.credentials(HTTP_AUTHORIZATION='Bearer ' + self.encode)
        profile = File(
            open('media/111466302_1173177489682337_5215215505022731993_o.jpg', 'rb'))
        data = {
            'avatar': profile,
            'country': 'Indonesia',
            'states': 'DIYogyakarta',
            'city': 'Yogyakarta',
            'address': 'Kaliwaru, Condongcatur, Kec. Depok, Kabupaten Sleman, Daerah Istimewa Yogyakarta 55281',
            'phone_numbers': '+6282228067652'
        }
        response = self.e.post(urls, data, foramt='multipart')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertNotEqual(response.data, None)
        log = logging.getLogger(__name__)
        log.info(response.data)
