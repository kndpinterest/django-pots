from rest_framework import serializers
from database.models.authenticate import CHOICE


class BaseUser(serializers.Serializer):
    username = serializers.CharField(required=False)
    first_name = serializers.CharField(required=False)
    last_name = serializers.CharField(required=False)
    email = serializers.EmailField(required=False)

    class Meta:
        abstract = True


class BaseAuthenticate(serializers.Serializer):
    avatar = serializers.ImageField(required=False)
    country = serializers.CharField(required=False)
    states = serializers.CharField(required=False)
    address = serializers.CharField(required=False)
    city = serializers.CharField(required=False)
    gender = serializers.IntegerField(default=CHOICE.male)
    phone_numbers = serializers.CharField(required=False)

    class Meta:
        abstract = True
