from rest_framework import serializers
from django.utils.translation import gettext as _
from django.contrib.auth.models import User
from pots_user.serializer.base import BaseAuthenticate, BaseUser
from database.models.authenticate import Stay


class Base(BaseUser, BaseAuthenticate):
    class Meta:
        abstract = True


class UserSerializer(Base):
    def __init__(self, instance=None, data=None, **kwargs):
        super().__init__(instance=instance, data=data, **kwargs)

    def get_fields(self, *args, **kwargs):
        fields = super(UserSerializer, self).get_fields(*args, **kwargs)
        return fields

    def create(self, validated_data):
        country = Stay(country=validated_data.get('country'), states=validated_data.get(
            'states'), city=validated_data.get('city'), address=validated_data.get('address'))
        country.save()
        check = self.context.get('user')
        if not check.authenticate_set.first():
            check.authenticate_set.create(
                avatar=validated_data.get('avatar'), country=country, phone_numbers=validated_data.get('phone_numbers'),
                gender=validated_data.get('gender'))
            self.context['message'] = _('Profle has been created')
            return self
        else:
            authenticate = check.authenticate_set.first()
            authenticate.avatar = validated_data.get(
                'avatar', authenticate.avatar)
            authenticate.phone_numbers = validated_data.get(
                'phone_numbers', authenticate.phone_numbers)
            authenticate.gender = validated_data.get(
                'gender', authenticate.gender)
            authenticate.save()
            country = authenticate.country
            country.country = validated_data.get('country')
            country.states = validated_data.get('states')
            country.city = validated_data.get('city')
            country.address = validated_data.get('address')
            country.save()
            self.context['message'] = _("Profile has been updated")
            return self

    def update(self, instance, validated_data):
        return validated_data
