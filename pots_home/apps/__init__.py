from django.apps import AppConfig


class PotsHomeConfig(AppConfig):
    name = 'pots_home'
