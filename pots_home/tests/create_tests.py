import logging
import unittest
from rest_framework import status
from rest_framework.test import APIClient
from django.contrib.auth.models import User
from django.core.files import File
from django.urls import reverse
from rest_framework_jwt.settings import api_settings
from database.models.experience import Experience
import random

jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER


title = ['What is Lorem Ipsum?', 'Why do we use it?', 'Where can I get some?']
description = ["It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).",
               "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.",
               "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes          from sections 1.10.32 and 1.10.33 of de Finibus Bonorum et Malorum (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, Lorem ipsum dolor sit amet.., comes from a line in section 1.10.32. The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from de Finibus Bonorum et Malorum by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham."]


class CreateTests(unittest.TestCase):
    def setUp(self):
        self.e = APIClient()
        self.payload = jwt_payload_handler(User.objects.first())
        self.encode = jwt_encode_handler(self.payload)

    def test_record_experience(self):
        self.e.credentials(HTTP_AUTHORIZATION='Bearer ' + self.encode)
        urls = reverse('api:experience-list')
        files = File(open('media/upwork (1).png', 'rb'))
        data = {
            'title': title[random.randint(0, 2)],
            'description': description[random.randint(0, 2)],
            'logo': files,
            'start': '20 November 2021 - 22 November 2022'
        }
        response = self.e.post(urls, data, format='multipart')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertNotEqual(response.data, None)
        log = logging.getLogger(__name__)
        log.info(response.data)

    @unittest.skipIf(Experience.objects.count() <= 4, 'Experience not have data')
    def test_destroy_experience(self):
        self.e.credentials(HTTP_AUTHORIZATION='Bearer ' + self.encode)
        experience = Experience.objects.first()
        urls = reverse('api:experience-detail', args=[experience.id])
        response = self.e.delete(urls, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertNotEqual(response.data, None)
        log = logging.getLogger(__name__)
        log.info(response.data)

    @unittest.skipIf(Experience.objects.count() == 0, 'Experience not have data')
    def test_update_experience(self):
        self.e.credentials(HTTP_AUTHORIZATION='Bearer ' + self.encode)
        experience = Experience.objects.first()
        urls = reverse('update-experience', args=[experience.id])
        files = File(open('media/upwork (1).png', 'rb'))
        data = {
            'title': title[random.randint(0, 2)],
            'description': description[random.randint(0, 2)],
            'logo': files
        }
        response = self.e.post(urls, data, format='multipart')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertNotEqual(response.data, None)
        log = logging.getLogger(__name__)
        log.info(response.data)

    def test_list_experience(self):
        self.e.credentials(HTTP_AUTHORIZATION="Bearer " + self.encode)
        urls = reverse('api:experience-list')
        response = self.e.get(urls, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertNotEqual(response.data, None)
        log = logging.getLogger(__name__)
        log.info(response.data)
