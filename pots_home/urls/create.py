from pots_home.views.experience import ExperienceModelViewSets, ExperienceAPIView
from django.urls import path, include
from rest_framework.routers import DefaultRouter

routers = DefaultRouter()
routers.register('experience', ExperienceModelViewSets, basename='experience')

urlpatterns = [
    path('', include((routers.urls, 'api'))),
    path('experience/update/<pk>/',
         ExperienceAPIView.as_view(), name='update-experience')
]
