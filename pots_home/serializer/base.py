from rest_framework import serializers


class BaseExperienceSerializer(serializers.Serializer):
    title = serializers.CharField(required=False)
    description = serializers.CharField(required=False)
    logo = serializers.ImageField(required=False)
    start = serializers.CharField(required=False)

    class Meta:
        abstract = True
