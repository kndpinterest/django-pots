from pots_home.serializer.base import BaseExperienceSerializer
from rest_framework import serializers
from database.models.experience import Experience
import os


class ExperienceSerializer(BaseExperienceSerializer):
    def __init__(self, instance=None, data=None, **kwargs):
        super().__init__(instance=instance, data=data, **kwargs)

    def get_fields(self, *args, **kwargs):
        fields = super(ExperienceSerializer, self).get_fields(*args, **kwargs)
        return fields

    def create(self, validated_data):
        experience = Experience(
            author=self.context.get('author'),
            title=validated_data.get('title'),
            description=validated_data.get('description'),
            logo=validated_data.get('logo'),
            start=validated_data.get('start')
        )
        experience.save()
        return validated_data

    def update(self, instance, validated_data):
        instance.title = validated_data.get('title', instance.title)
        instance.description = validated_data.get(
            'description', instance.description)
        if instance.logo.url:
            splits = str(instance.logo.url).split('/')
            filename = splits[len(splits) - 1]
            os.system('rm media/logo/%s' % filename)
        instance.logo = validated_data.get('logo', instance.logo)
        instance.start = validated_data.get('start', instance.start)
        instance.save()
        return instance


class ExperienceModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = Experience
        fields = "__all__"
