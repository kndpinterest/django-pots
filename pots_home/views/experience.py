from django.shortcuts import get_object_or_404
from pots_home.serializer.experience_serializer import ExperienceSerializer, ExperienceModelSerializer
from database.models.experience import Experience
from rest_framework import status, permissions, parsers
from rest_framework.viewsets import ModelViewSet
from rest_framework.views import APIView
from rest_framework.response import Response
from django.utils.translation import gettext as _


class ExperienceModelViewSets(ModelViewSet):
    permission_classes = [permissions.IsAdminUser, ]
    queryset = Experience.objects.all()
    serializer_func = ExperienceSerializer
    serializer_class = ExperienceModelSerializer
    parser_classes = [parsers.JSONParser, parsers.MultiPartParser, ]

    def create(self, request):
        user = request.user.authenticate_set.first()
        serializer = self.serializer_func(data=request.data)
        serializer.context['author'] = user
        if serializer.is_valid():
            serializer.save()
            return Response({'message': _("Experience has been created")}, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def destroy(self, request, pk):
        check = get_object_or_404(Experience, pk=pk)
        if not check:
            return Response({'message': _('Experience not found')}, status=status.HTTP_404_NOT_FOUND)
        check.delete()
        return Response({'message': _('Experience has been deleted')}, status=status.HTTP_200_OK)


class ExperienceAPIView(APIView):
    queryset = Experience.objects.all()
    serializer_class = ExperienceSerializer
    parser_classes = [parsers.JSONParser, parsers.MultiPartParser]

    def post(self, request, pk):
        check = get_object_or_404(Experience, pk=pk)
        serializer = self.serializer_class(check, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({'message': _('Experience has been updated')}, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
