from django.shortcuts import render
from django.contrib.auth.models import User
from django.views.generic import ListView
from django.views.generic.base import TemplateView
from django.conf import settings
import os
import json
from database.models.experience import Experience

nameImage = ['node', 'angular', 'vue', 'react']


class ListHome(TemplateView):
    template_name = "home.html"

    def get(self, request, *args, **kwargs):
        logo = '%sterminal-512.webp' % settings.MEDIA_URL
        me = "%s111466302_1173177489682337_5215215505022731993_o.jpg" % settings.MEDIA_URL
        avatar = "%s149829234_1345526515780766_5756524061471530890_o.png" % settings.MEDIA_URL
        email = "%scontact/contact-icon-mail.png" % settings.MEDIA_URL
        contact = "%scontact/contact-icon-phone.png" % settings.MEDIA_URL
        address = "%scontact/contact-icon-pin.png" % settings.MEDIA_URL
        pro = "%swebsite_programming.png" % settings.MEDIA_URL
        experience = Experience.objects.all()
        user = User.objects.first()

        return render(self.request, self.template_name, {"logo": logo, "me": me, 'avatar': avatar, "contact": contact, 'address': address, 'email': email, 'pro': pro, 'experience': experience, 'user': user})
