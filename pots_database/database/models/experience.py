from django.db import models
from .authenticate import Authenticate
from django.utils import timezone


class Experience(models.Model):
    author = models.ForeignKey(Authenticate, on_delete=models.CASCADE)
    title = models.CharField(max_length=225, null=False)
    description = models.TextField(null=False)
    start = models.CharField(max_length=225, null=False)
    logo = models.ImageField(upload_to="logo/")
    createAt = models.DateTimeField(auto_now_add=True)
    updateAt = models.DateTimeField()

    def save(self, *args, **kwargs):
        self.updateAt = timezone.now()
        super().save(*args, **kwargs)
