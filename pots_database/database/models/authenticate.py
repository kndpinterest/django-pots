from django.utils import timezone
from django.db import models
from .base import Base
from django.utils.translation import gettext as _
from phonenumber_field.modelfields import PhoneNumberField

class CHOICE:
    male = 1
    female = 2
    CHOICE = (
            (male,_("Male")),
            (female,_("Female")),
            )

class Stay(models.Model):
    country = models.CharField(max_length=225, null=False)
    states = models.CharField(max_length=225, null=False)
    address = models.CharField(max_length=225, null=False)
    city = models.CharField(max_length=225, null=False)

class Authenticate(Base):
    gender = models.IntegerField(choices=CHOICE.CHOICE, default=CHOICE.male)
    avatar = models.ImageField(upload_to="avatar/")
    phone_numbers = PhoneNumberField(null=False)
    country = models.ForeignKey(Stay, on_delete=models.CASCADE, related_name='stay_author_relation', null=True)


    def save(self, *args, **kwargs):
        self.updateAt = timezone.now()
        super().save(*args, **kwargs)
