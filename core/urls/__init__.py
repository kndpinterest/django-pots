from django.urls import path, include


urlpatterns = [
    path('', include('pots_home.urls.home')),
    path('record/', include('pots_home.urls.create')),
    path('user/', include('pots_user.urls.user'))
]
